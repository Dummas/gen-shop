<?php
// Locale
$_['code']                  = 'lt';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Taip';
$_['text_no']               = 'Ne';
$_['text_none']             = ' --- Neegzistuoja --- ';
$_['text_select']           = ' --- Pasirinkti --- ';
$_['text_all_zones']        = 'Visos zonos';
$_['text_pagination']       = 'Showing %d to %d of %d (%d Pages)';
$_['text_loading']          = 'Kraunasi...';

// Buttons
$_['button_address_add']    = 'Pridėti adresą';
$_['button_back']           = 'Atgal';
$_['button_continue']       = 'Tęsti';
$_['button_cart']           = 'Pridėti į krepšelį';
$_['button_cancel']         = 'Atšaukti';
$_['button_compare']        = 'Palyginti produktą';
$_['button_wishlist']       = 'Pridėti į norų sąrašą';
$_['button_checkout']       = 'Krepšelis';
$_['button_confirm']        = 'Patvirtinti užsakymą';
$_['button_coupon']         = 'Pritaikyti kuponą';
$_['button_delete']         = 'Ištrinti';
$_['button_download']       = 'Atsisiųsti';
$_['button_edit']           = 'Redaguoti';
$_['button_filter']         = 'Refine Search';
$_['button_new_address']    = 'Naujas adresas';
$_['button_change_address'] = 'Adreso pakeitimas';
$_['button_reviews']        = 'Apžvalgos';
$_['button_write']          = 'Parašyti apžvalgą';
$_['button_login']          = 'Prisijungti';
$_['button_update']         = 'Atnaujinti';
$_['button_remove']         = 'Pašalinti';
$_['button_reorder']        = 'Reorder';
$_['button_return']         = 'Grąžinti';
$_['button_shopping']       = 'Tęsti apsipirkimą';
$_['button_search']         = 'Ieškoti';
$_['button_shipping']       = 'Taikyti pristatymą';
$_['button_submit']         = 'Pateikti';
$_['button_guest']          = 'Svečio krepšelis';
$_['button_view']           = 'Žiūrėti';
$_['button_voucher']        = 'Pritaikyti kuponą';
$_['button_upload']         = 'Įkelti bylą';
$_['button_reward']         = 'Pritaikyti taškus';
$_['button_quote']          = 'Get Quotes';
$_['button_list']           = 'Sąrašas';
$_['button_grid']           = 'Tinklelis';
$_['button_map']            = 'View Google Map';

// Error
$_['error_exception']       = 'Error Code(%s): %s in %s on line %s';
$_['error_upload_1']        = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']        = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']        = 'Warning: The uploaded file was only partially uploaded!';
$_['error_upload_4']        = 'Warning: No file was uploaded!';
$_['error_upload_6']        = 'Warning: Missing a temporary folder!';
$_['error_upload_7']        = 'Warning: Failed to write file to disk!';
$_['error_upload_8']        = 'Warning: File upload stopped by extension!';
$_['error_upload_999']      = 'Warning: No error code available!';
