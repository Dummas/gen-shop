<?php
// Text
$_['text_refine']       = 'Refine Search';
$_['text_product']      = 'Produktai';
$_['text_error']        = 'Kategorijos rasti nepavyko';
$_['text_empty']        = 'There are no products to list in this category.';
$_['text_quantity']     = 'Kiekis:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Produkto kodas:';
$_['text_points']       = 'Reward Points:';
$_['text_price']        = 'Kaina:';
$_['text_tax']          = 'Be PVM:';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_sort']         = 'Rikiuoti:';
$_['text_default']      = 'Numatyta';
$_['text_name_asc']     = 'Vardas (A - Z)';
$_['text_name_desc']    = 'Vardas (Z - A)';
$_['text_price_asc']    = 'Kaina (Low &gt; High)';
$_['text_price_desc']   = 'Kaina (High &gt; Low)';
$_['text_rating_asc']   = 'Įvertinimas (Lowest)';
$_['text_rating_desc']  = 'Įvertinimas (Highest)';
$_['text_model_asc']    = 'Modelis (A - Z)';
$_['text_model_desc']   = 'Modelis (Z - A)';
$_['text_limit']        = 'Rodyti:';
$_['text_information']  = 'Informacija';