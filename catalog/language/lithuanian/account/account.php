<?php
// Heading
$_['heading_title']      = 'Naudotojas';

// Text
$_['text_account']       = 'Naudotojas';
$_['text_my_account']    = 'Mano naudotojas';
$_['text_my_orders']     = 'Mano užsakymai';
$_['text_my_newsletter'] = 'Naujienlaiškis';
$_['text_edit']          = 'Keisti naudotojo informaciją';
$_['text_password']      = 'Keisti slaptažodį';
$_['text_address']       = 'Keisti adresą';
$_['text_wishlist']      = 'Keisti norų sąrašą';
$_['text_order']         = 'Peržiūrėti užsakymų istoriją';
$_['text_download']      = 'Atsisiuntimai';
$_['text_reward']        = 'Sukaupti taškai';
$_['text_return']        = 'Peržiūrėti grąžinimo užklausas';
$_['text_transaction']   = 'Tavo sandoriai';
$_['text_newsletter']    = 'Naujienlaiškio prenumerata';
$_['text_recurring']     = 'Pasikartojantys mokėjimai';
$_['text_transactions']  = 'Sandoriai';