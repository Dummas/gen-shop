<?php
// Heading
$_['heading_title'] = 'Norų sąrašas';

// Text
$_['text_account']  = 'Naudotojas';
$_['text_instock']  = 'Yra sandelyje';
$_['text_wishlist'] = 'Wish List (%s)';
$_['text_login']    = 'You must <a href="%s">login</a> or <a href="%s">create an account</a> to save <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_success']  = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_exists']   = '<a href="%s">%s</a> is already in your <a href="%s">wish list</a>!';
$_['text_remove']   = 'Success: You have modified your wish list!';
$_['text_empty']    = 'Norų sąrašas yra tuščias.';

// Column
$_['column_image']  = 'Iliustracija';
$_['column_name']   = 'Pavadinimas';
$_['column_model']  = 'Modelis';
$_['column_stock']  = 'Stock';
$_['column_price']  = 'Unit Price';
$_['column_action'] = 'Veiksmas';