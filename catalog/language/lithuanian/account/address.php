<?php
// Heading
$_['heading_title']        = 'Adresų knyga';

// Text
$_['text_account']         = 'Naudotojas';
$_['text_address_book']    = 'Adresų knyga';
$_['text_edit_address']    = 'Keisti adresą';
$_['text_add']             = 'Your address has been successfully inserted';
$_['text_edit']            = 'Your address has been successfully updated';
$_['text_delete']          = 'Your address has been successfully deleted';
$_['text_empty']           = 'You have no addresses in your account.';

// Entry
$_['entry_firstname']      = 'Vardas';
$_['entry_lastname']       = 'Pavardė';
$_['entry_company']        = 'Kompanija';
$_['entry_address_1']      = 'Adresas 1';
$_['entry_address_2']      = 'Adresas 2';
$_['entry_postcode']       = 'Pašto kodas';
$_['entry_city']           = 'Miestas';
$_['entry_country']        = 'Šalis';
$_['entry_zone']           = 'Regionas';
$_['entry_default']        = 'Numatytas adresas';

// Error
$_['error_delete']         = 'Warning: You must have at least one address!';
$_['error_default']        = 'Warning: You can not delete your default address!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_vat']            = 'VAT number is invalid!';
$_['error_address_1']      = 'Address must be between 3 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_custom_field']   = '%s required!';
