<?php
// Heading
$_['heading_title']      = 'Naudotojo informacija';

// Text
$_['text_account']       = 'Naudotojas';
$_['text_edit']          = 'Keisti informaciją';
$_['text_your_details']  = 'Asmeninė informaciją';
$_['text_success']       = 'Sekmė: Informacija buvo atnaujinta.';

// Entry
$_['entry_firstname']    = 'Vardas';
$_['entry_lastname']     = 'Pavardė';
$_['entry_email']        = 'Elektroninis paštas';
$_['entry_telephone']    = 'Telefonas';
$_['entry_fax']          = 'Faksas';

// Error
$_['error_exists']       = 'Warning: E-Mail address is already registered!';
$_['error_firstname']    = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 1 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']    = 'Telephone must be between 3 and 32 characters!';
$_['error_custom_field'] = '%s required!';
