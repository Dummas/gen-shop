<?php
// Heading
$_['heading_title'] = 'Naudotojas sėkmingai sukurtas!';

// Text
//$_['text_message']  = '<p>Congratulations! Your new account has been successfully created!</p> <p>You can now take advantage of member privileges to enhance your online shopping experience with us.</p> <p>If you have ANY questions about the operation of this online shop, please e-mail the store owner.</p> <p>A confirmation has been sent to the provided e-mail address. If you have not received it within the hour, please <a href="%s">contact us</a>.</p>';
$_['text_message']  = '<p>Sveikiname! Jūsų naudotojas sėkmingai sukurtas</p> <p>Dabar sėkmingai galite naudotis visomis registruoto naudotojo privilegijomis.</p> <p>Jeigu turite bent kažkokių klausimų, prašome kreiptis pas mus.</p> <p>Patvirtinimo laiškas buvo išsiųstas į jūsų pašto dežutę. Jeigu laiško negavote po valandos, prašome <a href="%s">susisiekti su mumis</a>.</p>';
$_['text_approval'] = '<p>Dėkojame už registraciją, %s!</p><p>Jūs busite informuotas, kuomet Jūsų naudotojas bus aktyvuotas mūsų patyrusios komandos.</p><p>Jeigu turite bent kažkokių klausimų, prašome <a href="%s">kreiptis pas mus</a>.</p>';
$_['text_account']  = 'Naudotojas';
$_['text_success']  = 'Sekmingai';
