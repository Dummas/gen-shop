<?php
// Heading
$_['heading_title']  = 'Slaptažodžio keitimas';

// Text
$_['text_account']   = 'Naudotojas';
$_['text_password']  = 'Slaptažodis';
$_['text_success']   = 'Sekmė: Slaptažodis buvo pakeistas.';

// Entry
$_['entry_password'] = 'Slaptažodis';
$_['entry_confirm']  = 'Slaptažodžio patvirtinimas';

// Error
$_['error_password'] = 'Password must be between 4 and 20 characters!';
$_['error_confirm']  = 'Password confirmation does not match password!';