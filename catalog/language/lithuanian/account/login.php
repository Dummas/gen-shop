<?php
// Heading
$_['heading_title']                = 'Naudotojo prisijungimas';

// Text
$_['text_account']                 = 'Naudotojas';
$_['text_login']                   = 'Prisijungimas';
$_['text_new_customer']            = 'Naujas naudotojas';
$_['text_register']                = 'Naudotojo Registracija';
$_['text_register_account']        = 'Sukūrus naudotojo profilį, galėsite greičiau apsipirkinėti, stebėti užsakymo vykdymo eigą ir sekti savo užsakymų istoriją';
//$_['text_register_account']        = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_returning_customer']      = 'Grįžtantis pirkėjas';
$_['text_i_am_returning_customer'] = 'Esu grįžtantis pirkėjas';
$_['text_forgotten']               = 'Pamiršau slaptažodį';

// Entry
$_['entry_email']                  = 'Elektroninio pašto adresas';
$_['entry_password']               = 'Slaptažodis';

// Error
$_['error_login']                  = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_attempts']               = 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour.';
$_['error_approved']               = 'Warning: Your account requires approval before you can login.';