<?php
// Heading
$_['heading_title']        = 'Naudotojo registracija';

// Text
$_['text_account']         = 'Naudotojas';
$_['text_register']        = 'Registracija';
$_['text_account_already'] = 'Jeigu jau turite naudotoją, prašome prisijungti <a href="%s">prisijngimo puslapyje</a>.';
$_['text_your_details']    = 'Asmeninė informacija';
$_['text_your_address']    = 'Adresas';
$_['text_newsletter']      = 'Naujienlaiškis';
$_['text_your_password']   = 'Slaptažodis';
$_['text_agree']           = 'Sutinku su <a href="%s" class="agree"><b>%s</b></a> sąlygomis';

// Entry
$_['entry_customer_group'] = 'Naudotojo grupė';
$_['entry_firstname']      = 'Vardas';
$_['entry_lastname']       = 'Pavardė';
$_['entry_email']          = 'Elektroninis paštas';
$_['entry_telephone']      = 'Telefonas';
$_['entry_fax']            = 'Faksas';
$_['entry_company']        = 'Kompanija';
$_['entry_address_1']      = 'Adresas 1';
$_['entry_address_2']      = 'Adresas 2';
$_['entry_postcode']       = 'Pašto kodas';
$_['entry_city']           = 'Miestas';
$_['entry_country']        = 'Šalis';
$_['entry_zone']           = 'Regionas';
$_['entry_newsletter']     = 'Užsiprenumeruoti';
$_['entry_password']       = 'Slaptažodis';
$_['entry_confirm']        = 'Slaptažodžio patvirtinimas';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_custom_field']   = '%s required!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_agree']          = 'Warning: You must agree to the %s!';