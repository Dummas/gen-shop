<?php
// Text
$_['text_title']				= 'Banko pavedimu';
$_['text_instruction']			= 'Banko pavedimo instrukcijos';
$_['text_description']			= 'Prašome pervesti visą sumą į banko naudotojo sąskaitą.';
$_['text_payment']				= 'Užsakymas nebus pristatytas tol, kol nebus atliktas mokėjimas.';