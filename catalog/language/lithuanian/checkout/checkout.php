<?php
// Heading
$_['heading_title']                  = 'Atsiskaitymas';

// Text
$_['text_cart']                      = 'Krepšelis';
$_['text_checkout_option']           = 'Žingsnis 1: Atsiskaitymo informacija';
$_['text_checkout_account']          = 'Žingsnis 2: Account &amp; Billing Details';
$_['text_checkout_payment_address']  = 'Žingsnis 2: Mokėjimo informacija';
$_['text_checkout_shipping_address'] = 'Žingsnis 3: Pristatymo dėtalės';
$_['text_checkout_shipping_method']  = 'Žingsnis 4: Pristatymo metodas';
$_['text_checkout_payment_method']   = 'Žingsnis 5: Atsiskaitymo metodas';
$_['text_checkout_confirm']          = 'Žingsnis 6: Patvirtinti užsakymą';
$_['text_modify']                    = 'Atnaujinti &raquo;';
$_['text_new_customer']              = 'Naujas klientas';
$_['text_returning_customer']        = 'Grįžtantis klientas';
$_['text_checkout']                  = 'Atsiskaitymo informacija:';
$_['text_i_am_returning_customer']   = 'Esu grįžtantis klientas';
$_['text_register']                  = 'Naudotojo registracija';
$_['text_guest']                     = 'Svečio atsiskaitymas';
$_['text_register_account']          = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_forgotten']                 = 'Pamiršau slaptažodį';
$_['text_your_details']              = 'Asmeninė informaciją';
$_['text_your_address']              = 'Adresas';
$_['text_your_password']             = 'Slaptažodis';
$_['text_agree']                     = 'Perskaičiau ir susipažinau su <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Noriu naudoti naują dresą';
$_['text_address_existing']          = 'Noriu naudoti egzistuojantį adresą';
$_['text_shipping_method']           = 'Pasirinkite priimtiną šio užsakymo pristatymo metodą.';
$_['text_payment_method']            = 'Pasirinkite priimtiną šio užsakymo apmokėjimo metodą.';
$_['text_comments']                  = 'Komentarai prie užsakymo';
$_['text_recurring_item']            = 'Recurring Item';
$_['text_payment_recurring']         = 'Payment Profile';
$_['text_trial_description']         = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description']       = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']            = '%s every %d %s(s) until canceled';
$_['text_day']                       = 'day';
$_['text_week']                      = 'week';
$_['text_semi_month']                = 'half-month';
$_['text_month']                     = 'month';
$_['text_year']                      = 'year';

// Column
$_['column_name']                    = 'Produktas';
$_['column_model']                   = 'Modelis';
$_['column_quantity']                = 'Kiekis';
$_['column_price']                   = 'Vieneto kaina';
$_['column_total']                   = 'Suma';

// Entry
$_['entry_email_address']            = 'E-Mail Address';
$_['entry_email']                    = 'E-Mail';
$_['entry_password']                 = 'Password';
$_['entry_confirm']                  = 'Password Confirm';
$_['entry_firstname']                = 'Vardas';
$_['entry_lastname']                 = 'Pavardė';
$_['entry_telephone']                = 'Telefonas';
$_['entry_fax']                      = 'Faksas';
$_['entry_address']                  = 'Pasirinkti adresą';
$_['entry_company']                  = 'Kompaniją';
$_['entry_customer_group']           = 'Naudotojo grupė';
$_['entry_address_1']                = 'Adresas 1';
$_['entry_address_2']                = 'Adresas 2';
$_['entry_postcode']                 = 'Pašto kodas';
$_['entry_city']                     = 'Miestas';
$_['entry_country']                  = 'Šalis';
$_['entry_zone']                     = 'Regionas';
$_['entry_newsletter']               = 'Noriu gauti %s naujienlaiškį.';
$_['entry_shipping'] 	             = 'Mano pristatymo ir apmokėjimo adresas yra toks pats.';

// Error
$_['error_warning']                  = 'There was a problem while trying to process your order! If the problem persists please try selecting a different payment method or you can contact the store owner by <a href="%s">clicking here</a>.';
$_['error_login']                    = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_attempts']                 = 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour.';
$_['error_approved']                 = 'Warning: Your account requires approval before you can login.';
$_['error_exists']                   = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']                = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']                 = 'Last Name must be between 1 and 32 characters!';
$_['error_email']                    = 'E-Mail address does not appear to be valid!';
$_['error_telephone']                = 'Telephone must be between 3 and 32 characters!';
$_['error_password']                 = 'Password must be between 3 and 20 characters!';
$_['error_confirm']                  = 'Password confirmation does not match password!';
$_['error_address_1']                = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                     = 'City must be between 2 and 128 characters!';
$_['error_postcode']                 = 'Postcode must be between 2 and 10 characters!';
$_['error_country']                  = 'Please select a country!';
$_['error_zone']                     = 'Please select a region / state!';
$_['error_agree']                    = 'Dėmesio: Jūs turite sutikti su %s!';
$_['error_address']                  = 'Warning: You must select address!';
$_['error_shipping']                 = 'Warning: Shipping method required!';
$_['error_no_shipping']              = 'Warning: No Shipping options are available. Please <a href="%s">contact us</a> for assistance!';
$_['error_payment']                  = 'Warning: Payment method required!';
$_['error_no_payment']               = 'Warning: No Payment options are available. Please <a href="%s">contact us</a> for assistance!';
$_['error_custom_field']             = '%s required!';