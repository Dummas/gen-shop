<?php
// Heading
$_['heading_title']        = 'Jūsų užsakymas sėkmingai pateiktas!';

// Text
$_['text_basket']          = 'Krepšelis';
$_['text_checkout']        = 'Atsiskaityti';
$_['text_success']         = 'Sekmė';
$_['text_customer']        = '<p>Jūsų užsakymas sėkmingai buvo pateiktas!</p><p>Galite peržiūrėti savo užsakymų istorija <a href="%s">naudotojo paskyros</a> puslapyje, paspaudus ant <a href="%s">istorijos</a>.</p><p>Jeigu turite kažkokių klausimų, prašome kreiptis į <a href="%s">mus</a>.</p><p>Dėkojame už pirkimą pas mus!</p>';
$_['text_guest']           = '<p>Jūsų užsakymas sėkmingai buvo pateiktas!</p><p>Jeigu turite kažkokių klausimų, prašome kreiptis į <a href="%s">mus</a>.</p><p>Dėkojame už pirkimą pas mus!</p>';
//$_['text_guest']           = '<p>Your order has been successfully processed!</p><p>Please direct any questions you have to the <a href="%s">store owner</a>.</p><p>Thanks for shopping with us online!</p>';