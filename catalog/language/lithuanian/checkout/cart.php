<?php
// Heading
$_['heading_title']            = 'Prekių krepšelis';

// Text
$_['text_success']             = 'Sekmė: pridėta prekė <a href="%s">%s</a> prie Tavo <a href="%s">prekių krepšelio</a>!';
$_['text_remove']              = 'Sekmė: krepšelis atnaujintas!';
$_['text_login']               = 'Attention: You must <a href="%s">login</a> or <a href="%s">create an account</a> to view prices!';
$_['text_items']               = '%s produktas(-tai) - %s';
$_['text_points']              = 'Uždirbami taškai: %s';
$_['text_next']                = 'Kas bus daroma toliau?';
$_['text_next_choice']         = 'Pasirinkti ar yra turimas nuolaido kodas ar taškai, kuriuos yra norima pritaikyti; ar norima paskaičiuoti pristatymo kainą';
//$_['text_next_choice']         = 'Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.';
$_['text_empty']               = 'Your shopping cart is empty!';
$_['text_day']                 = 'diena';
$_['text_week']                = 'savaitė';
$_['text_semi_month']          = 'pusė mėnesio';
$_['text_month']               = 'mėnuo';
$_['text_year']                = 'metai';
$_['text_trial']               = '%s every %s %s for %s payments then ';
$_['text_recurring']           = '%s every %s %s';
$_['text_length']              = ' for %s payments';
$_['text_until_cancelled']     = 'until cancelled';
$_['text_recurring_item']      = 'Recurring Item';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';

// Column
$_['column_image']             = 'Iliustracija';
$_['column_name']              = 'Produktas';
$_['column_model']             = 'Modelis';
$_['column_quantity']          = 'Kiekis';
$_['column_price']             = 'Vieneto kaina';
$_['column_total']             = 'Suma';

// Error
$_['error_stock']              = 'Products marked with *** are not available in the desired quantity or not in stock!';
$_['error_minimum']            = 'Minimum order amount for %s is %s!';
$_['error_required']           = '%s required!';
$_['error_product']            = 'Warning: There are no products in your cart!';
$_['error_recurring_required'] = 'Please select a payment recurring!';