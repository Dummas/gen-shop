<?php
// Text
$_['text_information']  = 'Informacija';
$_['text_service']      = 'Klientų aptarnavimas';
$_['text_extra']        = 'Papildoma';
$_['text_contact']      = 'Susisiekti';
$_['text_return']       = 'Grąžinimas';
$_['text_sitemap']      = 'Tinklapio žemėlapis';
$_['text_manufacturer'] = 'Prekių ženklai';
$_['text_voucher']      = 'Dovanų kuponai';
$_['text_affiliate']    = 'Filialai';
$_['text_special']      = 'Specialūs pasiūlymai';
$_['text_account']      = 'Mano naudotojas';
$_['text_order']        = 'Užsakymų istorija';
$_['text_wishlist']     = 'Norų sąrašas';
$_['text_newsletter']   = 'Naujienlaiškis';
$_['text_powered']      = 'Sprendimas <a href="http://www.nork.lt">Nork</a><br /> %s &copy; %s';