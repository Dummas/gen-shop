<?php
// Text
$_['text_home']          = 'Pagrindinis';
$_['text_wishlist']      = 'Norai (%s)';
$_['text_shopping_cart'] = 'Krepšelis';
$_['text_category']      = 'Kategorijos';
$_['text_account']       = 'Mano naudotojas';
$_['text_register']      = 'Registracija';
$_['text_login']         = 'Prisijungti';
$_['text_order']         = 'Užsakymai';
$_['text_transaction']   = 'Tranzakcijos';
$_['text_download']      = 'Atsisiuntimai';
$_['text_logout']        = 'Atsijungti';
$_['text_checkout']      = 'Atsiskaityti';
$_['text_search']        = 'Paieška';
$_['text_all']           = 'Visi';