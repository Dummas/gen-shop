<?php
// Text
$_['text_items']    = '%s produktas(-tai) - %s';
$_['text_empty']    = 'Pirkinių krepšelis yra tuščias';
$_['text_cart']     = 'Tavo krepšelis';
$_['text_checkout'] = 'Atsiskaityti';
$_['text_recurring']  = 'Mokėtojo profilis';