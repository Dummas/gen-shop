<?php
// HTTP
define('HTTP_SERVER', 'http://gen.nork.lt/admin/');
define('HTTP_CATALOG', 'http://gen.nork.lt/');

// HTTPS
define('HTTPS_SERVER', 'http://gen.nork.lt/admin/');
define('HTTPS_CATALOG', 'http://gen.nork.lt/');

// DIR
define('DIR_APPLICATION', '../admin/');
define('DIR_SYSTEM', '../system/');
define('DIR_LANGUAGE', '../admin/language/');
define('DIR_TEMPLATE', '../admin/view/template/');
define('DIR_CONFIG', '../system/config/');
define('DIR_IMAGE', '../image/');
define('DIR_CACHE', '../system/cache/');
define('DIR_DOWNLOAD', '../system/download/');
define('DIR_UPLOAD', '../system/upload/');
define('DIR_LOGS', '../system/logs/');
define('DIR_MODIFICATION', '../system/modification/');
define('DIR_CATALOG', '../catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'opencart');
define('DB_PREFIX', 'oc_');
