<?php
// HTTP
define('HTTP_SERVER', 'http://opencart.maksim.local/');

// HTTPS
define('HTTPS_SERVER', 'http://opencart.maksim.local/');

// DIR
define('DIR_APPLICATION', '/vagrant/Genimpeksas/opencart/upload/catalog/');
define('DIR_SYSTEM', '/vagrant/Genimpeksas/opencart/upload/system/');
define('DIR_LANGUAGE', '/vagrant/Genimpeksas/opencart/upload/catalog/language/');
define('DIR_TEMPLATE', '/vagrant/Genimpeksas/opencart/upload/catalog/view/theme/');
define('DIR_CONFIG', '/vagrant/Genimpeksas/opencart/upload/system/config/');
define('DIR_IMAGE', '/vagrant/Genimpeksas/opencart/upload/image/');
define('DIR_CACHE', '/vagrant/Genimpeksas/opencart/upload/system/cache/');
define('DIR_DOWNLOAD', '/vagrant/Genimpeksas/opencart/upload/system/download/');
define('DIR_UPLOAD', '/vagrant/Genimpeksas/opencart/upload/system/upload/');
define('DIR_MODIFICATION', '/vagrant/Genimpeksas/opencart/upload/system/modification/');
define('DIR_LOGS', '/vagrant/Genimpeksas/opencart/upload/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'opencart');
define('DB_PREFIX', 'oc_');
